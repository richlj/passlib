# passlib
passlib is a Go-based harm reduction program for credentials stored in the local [development] environment.

It uses the pass utility and the address scheme for that application.

It requires GUI passphrase management software such as GNOME Keyring for GPG decryption.

## Usage
```go
import "github.com/richlj/passlib"

password, err := pass.Get("http", "some", ".") 
/* Returns credentials matching the path
"^http/someAPI/anything$", where this is the only match */
```
